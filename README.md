# git-policy POC

This repo is automated to store and update policies in Anchore Enterprise. 
The policy file included here will automatically be updated if you apply
changes in this repo. Warning, it will overwrite any changes you make
in the Anchroe Enterprise policy UI.

How to setup:
1. Fork this repo 
1. Configure CI/CD environment variables for your Anchore Enterprise environment
1. Make changes to the policy file as needed.

Warnings
This intended for proof of concept only and should not be used in production. 
1. No policy checks are in place. This could push a badly formatted policy to Anchore
1. This will overwrite any changes you make in the UI. 